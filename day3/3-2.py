coords = {} 
with open("input.txt", "r") as input:
	directions = input.read()
current = currentR = [0, 0]
coords[0,0] = 2
count = 1
for direction in directions:
	if(direction == '<' and bool(count % 2)):
			current = [current[0] - 1, current[1]]
	elif(direction == '<' and not bool(count % 2)):
			currentR = [currentR[0] - 1, currentR[1]]
	elif(direction == '>' and bool(count % 2)):
			current = [current[0] + 1, current[1]]
	elif(direction == '>' and not bool(count % 2)):
			currentR = [currentR[0] + 1, currentR[1]]
	elif(direction == '^' and bool(count % 2)):
			current = [current[0], current[1] + 1]
	elif(direction == '^' and not bool(count % 2)):
			currentR = [currentR[0], currentR[1] + 1]
	elif(direction == 'v' and bool(count % 2)):
			current = [current[0], current[1] -1]
	elif(direction == 'v' and not bool(count % 2)):
			currentR = [currentR[0], currentR[1] -1]
	if(bool(count % 2)):
		coords[current[0], current[1]] = coords.get((current[0],current[1]), 0) + 1
	else:
		coords[currentR[0], currentR[1]] = coords.get((currentR[0],currentR[1]), 0) + 1
	count += 1
print len(coords.values())
