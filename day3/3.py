coords = {} 
with open("input.txt", "r") as input:
	directions = input.read()
current = [0, 0]
coords[0,0] = 1
for direction in directions:
	if(direction == '<'):
		current = [current[0] - 1, current[1]]
	elif(direction == '>'):
		current = [current[0] + 1, current[1]]
	elif(direction == '^'):
		current = [current[0], current[1] + 1]
	elif(direction == 'v'):
		current = [current[0], current[1] -1]
	coords[current[0], current[1]] = coords.get((current[0],current[1]), 0) + 1
print len(coords.values())
