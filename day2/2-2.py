totalLength = 0
with open("input.txt", "r") as input:
	for line in input:
		sides = [int(x) for x in line.rstrip().split('x')]
		totalLength += reduce(lambda x, y: x * y, sides) # bow
		sides.remove(max(sides))
		totalLength += sum(sides) * 2 # smallest perimeter
print totalLength
