totalArea = 0
with open("input.txt", "r") as input:
	for line in input:
		a = []
		a.append(int(line.split('x')[0]) * int(line.split('x')[1]))
		a.append(int(line.split('x')[1]) * int(line.replace('\n','').split('x')[2]))
		a.append(int(line.replace('\n', '').split('x')[2]) * int(line.split('x')[0]))
		totalArea += 2 * sum(a) + min(a)
print totalArea
