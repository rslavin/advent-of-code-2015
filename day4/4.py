import hashlib, sys, multiprocessing, time

with open("input.txt", "r") as f:
	code = f.read().rstrip()

lookingFor = "000000"
totalProcesses = multiprocessing.cpu_count()
processes = []
segmentSize= 100000

# split into n processes segmentSize apart
# once they reach segmentSize, increment (n - 1) * segmentSize and keep going
def findHash(input):
	startAt = input[0]
	i = input[1]
	sys.stdout.flush()
	segmentEnd = startAt + segmentSize
	while startAt < segmentEnd:
		if(hashlib.md5((code + str(startAt)).encode('utf-8')).hexdigest().startswith(lookingFor)):
			print startAt
			sys.stdout.flush()
		startAt += 1
	findHash([startAt + ((int(totalProcesses) - 1) * segmentSize), i])

params = ([0, 1], [1 * segmentSize, 2], [2 * segmentSize, 3], [3 * segmentSize, 4])
p = multiprocessing.Pool(totalProcesses)
for process in range(totalProcesses):
	print process
	sys.stdout.flush()
	p.map(findHash, params)
