import re

with open("input.txt", "r") as input:
	count = 0
	for line in input:
		if re.match(r"(?=.*(\w)\1+)(?=.*(.*?[aeiou]){3,})^((?!ab|cd|pq|xy).)*$", line):
			count += 1
print count
